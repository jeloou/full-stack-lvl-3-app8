var db = require('mongoose'),
    crypto = require('crypto');

var Schema = new db.Schema({
  username: {type: String, lowercase: true, trim: true, default: ''},
  hash: {type: String, default: ''},
  salt: {type: String, default: ''}
});

Schema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.genSalt();
    this.hash = this.genHash(password);
  })
  .get(function() {
    return this._password;
  });

Schema.methods.toJSON = function() {
  return {
    id: this._id,
    username: this.username
  };
};

Schema.methods.authenticate = function(password) {
  return this.genHash(password) === this.hash;
};

Schema.methods.genSalt = function() {
  return crypto
    .randomBytes(64)
    .toString('base64');
};

Schema.methods.genHash = function(password) {
  return crypto
    .pbkdf2Sync(password, this.salt, 10000, 128)
    .toString('base64');
};

Schema.statics.add = function(payload, fn) {
  var user;

  user = new(this)(payload);
  user.save(function(err) {
    if (err) throw err;
    fn(null, user);
  });
};

var User = db.model('User', Schema);
