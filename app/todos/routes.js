var db = require('mongoose'),
    Todo = db.model('Todo');

module.exports = (function(app) {
  app.route('/todos')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .post(function(req, res) {
      var args,
	  todo;

      todo = JSON.parse(req.body.model);
      args = {
	user: req.user._id,
	payload: todo
      };

      Todo.add(args, function(err, todo) {
	if (err) return res.json(err);
	res.json(todo);
      });
    })
    .get(function(req, res) {
      var args;

      args = {
	user: req.user._id
      };

      Todo.fetch(args, function(err, todos) {
	if (err) return res.json(err);
	res.json(todos);
      });
    });

  app.route('/todos/:id')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .get(function(req, res) {
      var args;

      args = {
	id: req.params.id,
	user: req.user._id
      };

      Todo.get(args, function(err, todo) {
	if (err) return res.json(err);
	res.json(todo);
      });
    })
    .put(function(req, res) {
      var args,
	  todo;

      todo = JSON.parse(req.body.model);
      args = {
	id: req.params.id,
	user: req.user._id,
	payload: todo
      };

      Todo.change(args, function(err, todo) {
	if (err) return res.json(err);
	res.json(todo);
      });
    })
    .delete(function(req, res) {
      var args;

      args = {
	id: req.params.id,
	user: req.user._id
      };

      Todo.remove(args, function(err, todo) {
	if (err) return res.json(err);
	res.json(todo);
      });
    });
});
