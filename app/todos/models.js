var db = require('mongoose');

var ObjectId = db.Schema.ObjectId;
var Schema = new(db.Schema)({
  title: {type: String, trim: true, required: true},
  user: {type: ObjectId, ref: 'User', required: true},
  done: {type: Boolean, default: false}
});

Schema.methods.toJSON = function() {
  return {
    id: this._id,
    title: this.title,
    done: this.done,
    user: this.user.toJSON()
  };
};

Schema.statics.add = function(args, fn) {

};

Schema.statics.get = function(args, fn) {

};

Schema.statics.fetch = function(args, fn) {

};

Schema.statics.change = function(args, fn) {

};

Schema.statics.remove = function(args, fn) {

};

var Todo = db.model('Todo', Schema);
