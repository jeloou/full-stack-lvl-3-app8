var title = 'Express.js Todos'

casper.test.begin('app', 3, function(test) {
  casper.start('http://localhost:8000/', function() {
    test.assertHttpStatus(200);
  });

  casper.then(function() {
    test.assertTitle(title, 'doesn\'t show the right title');
    test.assertExists('#todoapp', 'doesn\'t show the todos body');
  });

  casper.run(function() {
    test.done();
  });
});

casper.test.begin('app - create', 2, function(test) {
  casper.start();

  casper.then(function() {
    this
      .fillSelectors('#todoapp', {
	'#new-todo': 'buy some milk'
      })
      .sendKeys('#new-todo', this.page.event.key.Enter, {
	keepFocus: true
      })
      .waitFor(function() {
	return this.evaluate(function() {
	  return $('#todo-list').length > 0;
	});
      }, function() {
	test.assertEquals(this.fetchText('#todo-list li:first-child label'), 'buy some milk');
	test.assertEquals(this.fetchText('.todo-count>b'), '1');
      });
  });

  casper.run(function() {
    test.done();
  });
});

casper.test.begin('app - remove', 1, function(test) {
  casper.start();

  casper.then(function() {
    this
      .evaluate(function() { $('#todo-list li .destroy').show(); })

    this
      .click('#todo-list li .destroy');

    test.assertDoesntExist('#todo-list li');
  });

  casper.run(function() {
    this.capture('capture.jpg', undefined, {format: 'jpg', quality: 100});
    test.done();
  });
});
